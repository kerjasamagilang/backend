import argon2 from 'argon2';
import { User } from '../entities/User';
import { MyContext } from 'src/types';
import {
  Arg,
  Ctx,
  Field,
  Mutation,
  ObjectType,
  Query,
  Resolver,
} from 'type-graphql';
import { EntityManager } from '@mikro-orm/postgresql';
import { COOKIE_NAME, LUPA_PASSWORD_PREFIX } from '../constans';
import { UsernamePasswordInput } from './utils/UsernamePasswordInput';
import { validateRegister } from './utils/validateRegister';
import { sendEmail } from './utils/sendEmail';
import { v4 } from 'uuid';

@ObjectType()
class FieldError {
  @Field()
  field: string;

  @Field()
  message: string;
}

@ObjectType()
class UserResponse {
  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];

  @Field(() => User, { nullable: true })
  user?: User;
}

@Resolver()
export class UserResolver {
  @Mutation(() => UserResponse)
  async lupaPassword(
    @Arg('token') token: string,
    @Arg('passwordBaru') passwordBaru: string,
    @Ctx() { redis, em, req }: MyContext
  ): Promise<UserResponse> {
    if (passwordBaru.length <= 5) {
      return {
        errors: [
          {
            field: 'passwordBaru',
            message: 'password harus lebih dari 5',
          },
        ],
      };
    }
    const key = LUPA_PASSWORD_PREFIX;
    const userId = await redis.get(key + token);
    if (!userId) {
      return {
        errors: [
          {
            field: 'token',
            message: 'token telah kadaluarsa',
          },
        ],
      };
    }

    const user = await em.findOne(User, { id: parseInt(userId) });
    if (!user) {
      return {
        errors: [
          {
            field: 'token',
            message: 'User tidak ditemukan',
          },
        ],
      };
    }

    user.password = await argon2.hash(passwordBaru);
    await em.persistAndFlush(user);
    
    await redis.del(key);
    req.session.userId = user.id;
    return { user };
  }

  @Mutation(() => Boolean)
  async forgotPassword(
    @Arg('email') email: string,
    @Ctx() { em, redis }: MyContext
  ) {
    const user = await em.findOne(User, { email });
    if (!user) {
      // email tidak ada di database
      return true;
    }

    const token = v4();

    await redis.set(
      LUPA_PASSWORD_PREFIX + token,
      user.id,
      'exp',
      1000 * 60 * 60 * 24 * 1 // satu Hari
    );
    await sendEmail(
      email,
      `<a href="http://localhost:3000/lupa-password/${token}">Lupa password</a>`
    );
    return true;
  }

  @Query(() => User, { nullable: true })
  async me(@Ctx() { em, req }: MyContext) {
    console.log('session :', req.session);
    // Anda belum login
    if (!req.session.userId) {
      return null;
    }

    const user = await em.findOne(User, { id: req.session.userId });
    return user;
  }

  @Mutation(() => UserResponse)
  async register(
    @Arg('options') options: UsernamePasswordInput,
    @Ctx() { em, req }: MyContext
  ): Promise<UserResponse> {
    const errors = validateRegister(options);
    if (errors) {
      return { errors };
    }

    const hashedPassword = await argon2.hash(options.password);
    let user;
    try {
      const result = await (em as EntityManager)
        .createQueryBuilder(User)
        .getKnexQuery()
        .insert({
          username: options.username,
          email: options.email,
          password: hashedPassword,
          created_at: new Date(),
          updated_at: new Date(),
        })
        .returning('*');
      user = result[0];
    } catch (error) {
      if (error.code === '23505' || error.detail.includes('sudah digunakan')) {
        return {
          errors: [
            {
              field: 'username',
              message: 'Username sudah digunakan',
            },
          ],
        };
      }
    }
    req.session!.userId = user.id;

    return { user };
  }

  @Mutation(() => UserResponse)
  async login(
    @Arg('usernameOrEmail') usernameOrEmail: string,
    @Arg('password') password: string,
    @Ctx() { em, req }: MyContext
  ): Promise<UserResponse> {
    const user = await em.findOne(
      User,
      usernameOrEmail.includes('@')
        ? { username: usernameOrEmail }
        : { email: usernameOrEmail }
    );
    if (!user) {
      return {
        errors: [
          {
            field: 'usernameOrEmail',
            message: 'Pengguna tidak ditemukan',
          },
        ],
      };
    }

    const valid = await argon2.verify(user.password, password);
    if (!valid) {
      return {
        errors: [
          {
            field: 'password',
            message: 'Password salah',
          },
        ],
      };
    }

    req.session.userId = user.id;
    req.session.randomKey = 'kesalahan kamu';

    return {
      user,
    };
  }
  @Mutation(() => Boolean)
  logout(@Ctx() { req, res }: MyContext) {
    return new Promise(resolve =>
      req.session.destroy(err => {
        res.clearCookie(COOKIE_NAME);

        if (err) {
          console.log(err);
          resolve(false);
        }
        resolve(true);
      })
    );
  }
}
