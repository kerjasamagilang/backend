import { UsernamePasswordInput } from './UsernamePasswordInput';

export const validateRegister = (options: UsernamePasswordInput) => {
  const regExUsername = /^(?=^.{8,15}$)(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?!.*\s).*$/;
  const regExPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,15}$/;
  const regExEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  // USERNAME VALIDATE
  if (!options.username.match(regExUsername)) {
    return [
      {
        field: 'username',
        message:
          'Username minimal 8 - 15 karakter dan terdiri dari huruf besar, huruf kecil, angka dan spesial karakter',
      },
    ];
  }
  if (options.username.trim() === '') {
    return [
      {
        field: 'username',
        message: 'Username tidak boleh kosong',
      },
    ];
  }

  // EMAIL VALIDATE
  if(!options.email){
    return [
      {
        field: 'email',
        message: 'email tidak boleh kosong'
      }
    ]
  }
  if (!options.email.match(regExEmail)) {
    return [
      {
        field: 'email',
        message: 'email tidak valid',
      },
    ];
  }

  // PASSWORD VALIDATE
  if (options.password.trim() === '') {
    return [
      {
        field: 'password',
        message: 'Password tidak boleh kosong',
      },
    ];
  }
  if (!options.password.match(regExPassword)) {
    return [
      {
        field: 'password',
        message:
          'Password harus menggunakan huruf besar, kecil dan spesial karakter',
      },
    ];
  }
  return null;
};
