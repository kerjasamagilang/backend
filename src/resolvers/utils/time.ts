const time = (ms: number) => new Promise(res => setTimeout(res, ms));
