import { EntityManager, IDatabaseDriver, Connection } from '@mikro-orm/core';
import { Request, Response } from 'express';
import { SessionData, Session } from 'express-session';
import { Redis } from 'ioredis';

export type MyContext = {
  em: EntityManager<any> & EntityManager<IDatabaseDriver<Connection>>;
  res: Response;
  redis: Redis;
  req: Request & {
    session: Session &
      Partial<SessionData> & { userId?: number; randomKey?: string };
  };
};
